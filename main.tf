provider "aws" {
  region = "us-west-1"
}

resource "aws_instance" "ec2-1" {
  ami           = "ami-0d9858aa3c6322f73"
  instance_type = var.instance_type
  tags = {
    name = "ivan_ec2-new"
  }
}

variable "instance_type" {}


resource "aws_instance" "ec2" {
ami           = "ami-0d9858aa3c6322f73"
instance_type = var.instance_type
 tags = {
    Name = "Ivan_ec2_test"
  }
}
